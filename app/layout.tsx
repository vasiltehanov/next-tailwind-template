'use client';

import { ToastContainer } from "react-toastify";
import { SWRConfig } from "swr";
import { SessionProvider } from "next-auth/react";

import 'react-toastify/dist/ReactToastify.css';
import "../styles/globals.css";

export default function RootLayout({
  children
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css" />
      </head>

      <body>
        <SWRConfig
          value={{
            fetcher: (...args) => fetch(...args).then(res => res.json()),
            provider: () => new Map()
          }}
        >
          <SessionProvider>
            {children}
          </SessionProvider>

          <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
          />
        </SWRConfig>
      </body>
    </html>
  )
}