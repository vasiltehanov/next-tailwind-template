'use client';

import type { NextPage } from "next";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup';
import { ExclamationCircleIcon } from '@heroicons/react/20/solid'
import { signIn } from "next-auth/react";


const schema = yup.object().shape({
  topic: yup.string().required()
})

interface FormData extends yup.InferType<typeof schema> {}

const Home: NextPage = () => {
  const { 
    register, 
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      topic: ''
    },
    resolver: yupResolver(schema)
  });

  const onSubmit = (data: FormData) => {
    try {
      console.log(data);

      toast.success('Successfully submitted!')
    } catch (err) {
      toast.error(`Error submitting: ${err}`)
    }
  }

  return (
    <main>
      <form onSubmit={handleSubmit(onSubmit)}>

        <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        {/* We've used 3xl here, but feel free to try other max-widths based on your needs */}
        <div className="mx-auto max-w-3xl">
            <h1>Example form</h1>

            <div className="relative mt-2 rounded-md shadow-sm">
              <input
                type="text"
                className="block w-full rounded-md border-0 py-1.5 pr-10 text-red-900 ring-1 ring-inset ring-red-300 placeholder:text-red-300 focus:ring-2 focus:ring-inset focus:ring-red-500 sm:text-sm sm:leading-6"
                placeholder="Topic"
                {...register('topic')}
              />
              <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-3">
                <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" />
              </div>
            </div>
            {errors.topic ? (
              <p className="mt-2 text-sm text-red-600" id="email-error">
                {errors.topic.message}
              </p>
            ) : null}

            <button
              type="submit"
              className="rounded-md mt-2 bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Submit
            </button>
            <button onClick={() => signIn('google')}>LOGIN BATe</button>
          </div>
        </div>
      </form>
    </main>
  );
};

export default Home;